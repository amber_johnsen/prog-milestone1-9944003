﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task36
{
    class Program
    {
        static void Main(string[] args)
        {
            // a simple for loop with a counter, in the block has an if statement that gives feedback on whether the counter is equal to the index.
            //Give different feedback when the loop is complete.
           
            var counter = 10;
            var i = 0;

            for (i = 0; i <= counter; i++)
            {
           
                if (counter == i)
                {
                    Console.WriteLine("The counter is equal to the index.");
                }
                else
                {

                }
            }
            Console.WriteLine("Loop is complete.");


                }
            }
}
