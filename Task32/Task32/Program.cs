﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task32
{
    class Program
    {
        static void Main(string[] args)
        {
            var days = new string[7] { "monday 1", "tuesday 2", "wednesday 3", "thursday 4", "friday 5", "saturday 6", "sunday 7" };

            for (var i = 0; i < days.Length; i++)
            {
                Console.WriteLine(days[i]);
            }
        }
    }
}
