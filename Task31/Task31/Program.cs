﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task31
{
    class Program
    {
        static void Main(string[] args)
        {
            // Write a program that checks if a number is divisible by 3 and 4 without leaving any decimal places. Inform the user of the number

            Console.WriteLine("Please enter a number");
            var x = int.Parse(Console.ReadLine());
            var a = x % 3;
            var b = x % 4;


            if (a == 0)
            {
                if (b == 0)
                {
                    Console.WriteLine($"Your Number is divisable by 3 and 4");

                }
                else
                {
                    Console.WriteLine($"The number you entered is not divisable by both 3 and 4");
                }

            }
            else
            {
                Console.WriteLine($"The number you entered is not divisable by both 3 and 4");
            }
        }
    }
}
