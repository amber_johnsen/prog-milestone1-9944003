﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task30
{
    class Program
    {
        static void Main(string[] args)
        {
            //Take in 2 number from a user and show the difference when they are added as a string and as a number
            var a = 0;
            var b = 0;
            var c = 0;

            Console.WriteLine("");
            Console.WriteLine(" Hello, you will be required to enter 2 numbers, the difference between the 2 numbers will be displayed on the screen. Please enter the first number and press enter");
            a = int.Parse(Console.ReadLine());

            Console.Clear();

            Console.WriteLine("");
            Console.WriteLine("Thank you, Please type in the second number and press enter");
            b = int.Parse(Console.ReadLine());

            c = (a - b);

            Console.Clear();
            Console.WriteLine("");
            Console.WriteLine($"According to my calculations the difference left is {c}.  This was worked out when we subtracted {b} from {a}.");

            
        }
    }
}
