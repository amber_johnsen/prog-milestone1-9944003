﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task21
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create dictionary populate with strings, fill with months & days display months 31 days
            var calander = new Dictionary<string, int>();

            calander.Add("January", 31);
            calander.Add("Febuary", 28);
            calander.Add("March", 31);
            calander.Add("April", 30);
            calander.Add("May", 31);
            calander.Add("June", 30);
            calander.Add("July", 31);
            calander.Add("August", 31);
            calander.Add("September", 30);
            calander.Add("October", 31);
            calander.Add("November", 30);
            calander.Add("December", 31);

            foreach (KeyValuePair<string, int> month in calander)
            {
                if (month.Value.Equals(31))

                {
                    Console.WriteLine(month.Key.ToString() + " has " + month.Value.ToString() + " days ");

                }

                }
                }
        }
    }
