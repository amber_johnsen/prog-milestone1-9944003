﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK_04
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to the Temperature Converter.  Please type F for Fahrenheit or C for Celsius");

            var temperature = Console.ReadLine();

            switch (temperature)
            {
                case "F":
                    Console.WriteLine("Type in the temperature value you would like to convert from Fahrenheit to Celsius");
                    Double Fah = double.Parse(Console.ReadLine());
                    Double FtoC = ((Fah - 32) * .5556);
                    Console.WriteLine($"The Celsius value is {0}:", FtoC);
                    Console.ReadLine();
                    break;

                case "C":
                    Console.WriteLine("Type in the temperature value you would like to convert from Celsius to Fahrenheit");
                    double Cel = double.Parse(Console.ReadLine());
                    double CtoF = ((Cel * 1.8) + 32);
                    Console.WriteLine($"The Fahrenheit value is {0}:", CtoF);
                    Console.ReadLine();
                    break;

                default:
                    Console.WriteLine("You had to type in C if conversion from Celsius, or F if conversion from Fahrenhiet please run the program agian.");
                    break;
            }
    }
}
