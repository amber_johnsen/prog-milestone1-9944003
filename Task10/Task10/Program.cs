﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task10
{
    class Program
    {
        static void Main(string[] args)
        {

            int thisYear = 2016;
            var numberOfYears = 20;
            var leapYearNumber = 4;
            var leapYear = numberOfYears / leapYearNumber;

            Console.WriteLine("Every four years is a leap year.");
            Console.WriteLine($"This year, {thisYear}, is a leap year and we want to know how many more leap years will occur in the next {numberOfYears} years");
            Console.WriteLine();
            Console.WriteLine("The following calculation will give the number of leap years over the next 20 years...");
            Console.WriteLine($"{numberOfYears} / {leapYearNumber} = {leapYear} ");
            Console.WriteLine();
            Console.WriteLine($"In {numberOfYears} years, we will experience {leapYear} leap years.");
            Console.ReadLine();
        }
    }
}
