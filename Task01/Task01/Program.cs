﻿using System;

namespace Task01
{
    class MainClass
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, can you please type in your name");

            var name = Console.ReadLine();

            Console.WriteLine($"Hello {name}! Please type in your age");

            var age = 0;
            age = int.Parse(Console.ReadLine());

            Console.WriteLine($"{name}, your age is {age}");

        }
    }
}
