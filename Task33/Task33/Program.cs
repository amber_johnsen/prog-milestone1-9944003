﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task33
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please input the amount of students enrolled and the amount of tutorial groups needed.");
            var students = int.Parse(Console.ReadLine());
            var tutorials = (students / 28);
            Console.WriteLine($"{tutorials}");

        }
    }
}
