﻿using System;

namespace Task03
{
    class MainClass
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please type in K for Kilometer or M for Miles");

            var metric = Console.ReadLine();
            const double mile2km = 1.609344;
            const double km2mile = 0.621371;

            switch (metric)
            {
                case "K":
                    Console.WriteLine("Type in a number to convert KM to Miles");
                    var getKM = double.Parse(Console.ReadLine());
                    var roundingKM = System.Math.Round(getKM * km2mile, 2);
                    Console.WriteLine($"{getKM} KM is {roundingKM} Miles");
                    break;
             
                case "M":
                    Console.WriteLine("Type in a number to convert Miles to KM");
                    var getMile = double.Parse(Console.ReadLine());
                    var roundingMile = System.Math.Round(getMile * mile2km, 2);
                    Console.WriteLine($"{getMile} KM is {roundingMile} Miles");
                    break;

                default:
                    Console.WriteLine("Please use capital letter when selecting \"K\" for Kilometer or \"M\" for Miles - please run the program agian.");
                    break;
            }
        }
    }
}

