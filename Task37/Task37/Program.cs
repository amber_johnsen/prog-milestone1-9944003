﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task37
{
    class Program
    {
        static void Main(string[] args)
        {
            //  1. Write a program that calculates how many hours you should study per week for a 15 credit paper.
            //  Use the following data to create your app.


            //  a.There are 10 hours of study for every credit of the paper
            // b.Each week gives 5 contact hours(hours in lecture and tutorials)
            //  c.The Semester is 12 weeks long.
            double crds = 15;
            double crdhrs = 10;
            double inclass = 5;
            double wks = 12;

            double study = (((crdhrs * crds) - (inclass * 12)) / 12);

            Console.WriteLine($"{study}");

        }
    }
}
