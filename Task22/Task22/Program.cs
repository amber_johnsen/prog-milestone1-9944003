﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task22
{
    class Program
    {
        static void Main(string[] args)
        {

            // Task requires checking the values of a Key and print it if it is true 1.Create a dictionary filled with fruits & vegetables listed.
            //The key should be the name of the fruit / vegetable & the value should be whether they are a fruit or vegetable. 
            // a.Display to the user how many fruits there are and how many vegetables there are.
            //b.Also display which of the list are fruits
            var i = 0;
            var dict = new Dictionary<string, string>();

            dict.Add("Silverbeet", "Vegetable");
            dict.Add("Broccoli", "Vegetable");
            dict.Add("Apple", "Fruit");
            dict.Add("Kiwifruit", "Fruit");
            dict.Add("Pear", "Fruit");
            dict.Add("Carrot", "Vegetable");

            foreach (var x in dict)
            {
                if (x.Value == "Fruit")
                {
                    i++;
                }
            }
            Console.WriteLine($"There are {i} fruits in my dictionary");
        }
    }
}

