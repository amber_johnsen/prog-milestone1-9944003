﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task38
{
    class Program
    {
        static void Main(string[] args)
        {
            var i = 0;
            var counter = 12;

            Console.WriteLine("Please enter a number, and the division table from 1x to 12x will be displayed.");

            var x = Console.ReadLine();
            var z = double.Parse(x);

            for (i = 0; i < counter; i++)
            {
                var a = i + 1;
                var c = (z / a);
              Console.WriteLine($"{z} / {a} = {c}");
            }
        }
    }
}
