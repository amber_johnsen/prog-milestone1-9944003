﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK_10
{
    class Program
    {
        static void Main(string[] args)
        {
            int currentYear = 2016;
            var numOfYears = 20;
            var yearMath = 0.25;
            var leapYears = numOfYears * yearMath;
            Console.WriteLine($"This year it is {currentYear}");
            Console.WriteLine("A leap year occurs every four years.");
            Console.WriteLine("1 / 4 = 0.25");
            Console.WriteLine($"Therefore, {numOfYears} x {yearMath} = {leapYears}");
            Console.WriteLine($"Within the next {numOfYears} years, there will be {leapYears} leap years.");
            Console.ReadLine();

        }
    }
}
